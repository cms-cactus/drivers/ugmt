#ifndef _ugmt_mu_quad_node_hpp_
#define _ugmt_mu_quad_node_hpp_

#include "ugmt/MuLUTManagerNode.hpp"
#include "uhal/DerivedNode.hpp"

namespace ugmt {

class MuQuadNode : public uhal::Node, public MuLUTManagerNode {
  UHAL_DERIVEDNODE(MuQuadNode);

public:
  MuQuadNode(const uhal::Node &aNode);
  virtual ~MuQuadNode();
  uint32_t bc0Errors() const;
  uint32_t bc0Errors(unsigned int i) const;
  uint32_t bxCntrErrors() const;
  uint32_t bxCntrErrors(unsigned int i) const;
  uint32_t muonCntr(unsigned int i) const;
  uint32_t muonCntrQuad() const;
  uint32_t oneNominalShowerCntr(unsigned int i) const;
  uint32_t oneNominalShowerCntrQuad() const;
  uint32_t oneTightShowerCntr(unsigned int i) const;
  uint32_t oneTightShowerCntrQuad() const;
  uint32_t oneLooseShowerCntr(unsigned int i) const;
  uint32_t oneLooseShowerCntrQuad() const;
  void clear() const;

private:
};
} // namespace ugmt

#endif /* */
