#ifndef _ugmt_lumi_counter_node_hpp_
#define _ugmt_lumi_counter_node_hpp_

#include "uhal/DerivedNode.hpp"

namespace ugmt {

class LumiCounterNode : public uhal::Node {
  UHAL_DERIVEDNODE( LumiCounterNode );
  public:
    LumiCounterNode( const uhal::Node& aNode );
    virtual ~LumiCounterNode();
    bool manualCntrReset() const;
    uint32_t lumiSectCntr() const;
    void clear() const;

  private:
};
} // namespace ugmt

#endif /* */
