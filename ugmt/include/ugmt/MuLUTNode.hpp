#ifndef _ugmt_mu_lut_node_hpp_
#define _ugmt_mu_lut_node_hpp_

// uHAL Headers
#include "uhal/DerivedNode.hpp"
#include <boost/property_tree/ptree.hpp>


using boost::property_tree::ptree;

namespace ugmt {

class MuLUTNode : public uhal::Node {
  UHAL_DERIVEDNODE( MuLUTNode );
  public:
    MuLUTNode( const uhal::Node& aNode );
    virtual ~MuLUTNode();
    void readHwContents(std::vector<uint32_t>& res) const;

    void writeContents(const std::vector<uint32_t>& contents) const;

    bool contentCorrect(const std::vector<uint32_t>& expContents) const;

  private:
};
} // namespace ugmt

#endif /* */
