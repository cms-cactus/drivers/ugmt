#ifndef _ugmt_mu_lut_manager_node_hpp_
#define _ugmt_mu_lut_manager_node_hpp_

#include <string>
#include <vector>
#include "xdata/UnsignedInteger.h"
#include "xdata/Vector.h"

// uHAL Headers
#include "uhal/Node.hpp"

namespace ugmt {

class MuLUTManagerNode {
  public:
    MuLUTManagerNode( const uhal::Node& aNode, const std::string& idPattern=".*mem_.*" );
    virtual ~MuLUTManagerNode();

    void setContent(xdata::Vector<xdata::UnsignedInteger>& lutData);
    void writeLUTs();
    bool contentsCorrect() const;

  private:
    std::string content_;
    const uhal::Node& node_;
    std::vector<uint32_t> lutContent_;
    std::vector<std::string> lutIds_;
};
} // namespace ugmt

#endif /* */
