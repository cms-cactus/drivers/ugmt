#include "ugmt/LumiCounterNode.hpp"

namespace ugmt {

UHAL_REGISTER_DERIVED_NODE(LumiCounterNode);

LumiCounterNode::LumiCounterNode(const uhal::Node& aNode) : uhal::Node(aNode)
{
}


LumiCounterNode::~LumiCounterNode()
{
}

bool LumiCounterNode::manualCntrReset() const
{
    uhal::ValWord<uint32_t> manualCntrReset = getNode("manual_reset_sel.enable_manual").read();

    getClient().dispatch();

    if (manualCntrReset != 0) {
        return true;
    }

    return false;
}

uint32_t LumiCounterNode::lumiSectCntr() const
{
    uhal::ValWord<uint32_t> lumiSectCntr = getNode("lumi_section_cnt").read();

    getClient().dispatch();

    return (uint32_t)(lumiSectCntr);
}

}
