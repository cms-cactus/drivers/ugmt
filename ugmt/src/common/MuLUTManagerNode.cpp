#include "ugmt/MuLUTManagerNode.hpp"
#include "ugmt/MuLUTNode.hpp"

// boost headers
#include "boost/foreach.hpp"

namespace ugmt {

MuLUTManagerNode::MuLUTManagerNode(const uhal::Node& aNode, const std::string& idPattern) :
    content_(aNode.getParameters().at("content")),
    node_(aNode),
    lutContent_(1,0)
{
    //std::cout << "Trying to find keys with pattern " << idPattern << " in node " << node_.getId() << std::endl;
    lutIds_ = node_.getNodes(idPattern);
    //BOOST_FOREACH(std::string& lutId, lutIds_) {
    //    std::cout << "Found LUT Id: " << lutId << std::endl;
    //}
    //std::cout << "Finished constructing LUT manager node" << std::endl;
}

void MuLUTManagerNode::setContent(xdata::Vector<xdata::UnsignedInteger>& lutData)
{
    lutContent_.clear();
    for (size_t i = 0; i < lutData.elements(); ++i) {
        lutContent_.emplace_back(((xdata::UnsignedInteger*)lutData.elementAt(i))->value_);
    }
}


MuLUTManagerNode::~MuLUTManagerNode()
{

}


void MuLUTManagerNode::writeLUTs()
{
    //std::cout << "Writing LUTs" << std::endl;
    BOOST_FOREACH(std::string& lutId, lutIds_) {
        //std::cout << "LUT Id: " << lutId << std::endl;
        const MuLUTNode& lut = node_.getNode<MuLUTNode>(lutId);
        lut.writeContents(lutContent_);
    }
}

bool MuLUTManagerNode::contentsCorrect() const
{
    bool result = true;

    //std::cout << "Checking LUT contents" << std::endl;
    BOOST_FOREACH(const std::string& lutId, lutIds_) {
        //std::cout << "LUT Id: " << lutId << std::endl;
        const MuLUTNode& lut = node_.getNode<MuLUTNode>(lutId);
        result = result && lut.contentCorrect(lutContent_); // collect IDs and return those?
    }
    return result;
}

}
