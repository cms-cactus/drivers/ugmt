#include "uhal/ValMem.hpp"
#include "ugmt/MuLUTNode.hpp"
#include <algorithm>

namespace ugmt {

UHAL_REGISTER_DERIVED_NODE(MuLUTNode);

MuLUTNode::MuLUTNode(const uhal::Node& aNode) :
    uhal::Node(aNode)
{
}

MuLUTNode::~MuLUTNode()
{
}

void MuLUTNode::readHwContents(std::vector<uint32_t>& hwContent) const
{
    uhal::ValVector<uint32_t> cntnts = readBlock(getSize());
    getClient().dispatch();

    hwContent = cntnts.value();
}

bool MuLUTNode::contentCorrect(const std::vector<uint32_t>& expContents) const
{
    std::vector<uint32_t> hwContent;
    readHwContents(hwContent);
    bool equal (
        std::equal (hwContent.begin(), hwContent.end(), expContents.begin())
    );
    //if (!equal) {
    //    for(std::vector<uint32_t>::const_iterator it = expContents.begin(); it != expContents.end(); ++it) {
    //        std::cout << (*it) << ", ";
    //    }
    //    std::cout << std::endl;
    //    for(std::vector<uint32_t>::iterator it = hwContent.begin(); it != hwContent.end(); ++it) {
    //        std::cout << (*it) << ", ";
    //    }
    //    std::cout << std::endl;
    //}
    return equal;
}

void MuLUTNode::writeContents(const std::vector<uint32_t>& contents) const
{
    if (contents.size() == getSize()) {
        //std::cout << "Writing LUT with size " << contents.size() << std::endl;
        if (!writeBlock(contents).valid()) {
            //std::cout << "Header invalid" << std::endl;
        }
        // for(std::vector<uint32_t>::const_iterator it = contents.begin(); it != contents.end(); ++it) {
        //     std::cout << (*it) << ", ";
        // }
        // std::cout << std::endl;
        getClient().dispatch();
    } else {
        std::cout << "meh... wrong size. LUT to write: " << contents.size() << ", Node: " << getSize() << std::endl;
    }
}

}
