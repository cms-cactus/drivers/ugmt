#include "ugmt/MuQuadNode.hpp"
#include <sstream>

namespace ugmt {

UHAL_REGISTER_DERIVED_NODE(MuQuadNode);

MuQuadNode::MuQuadNode(const uhal::Node &aNode)
    : uhal::Node(aNode),
      MuLUTManagerNode(dynamic_cast<const uhal::Node &>(*this)) {}

MuQuadNode::~MuQuadNode() {}

uint32_t MuQuadNode::bc0Errors() const {
  uhal::ValWord<uint32_t> err0 = getNode("bc0_errors_0").read();
  uhal::ValWord<uint32_t> err1 = getNode("bc0_errors_1").read();
  uhal::ValWord<uint32_t> err2 = getNode("bc0_errors_2").read();
  uhal::ValWord<uint32_t> err3 = getNode("bc0_errors_3").read();

  getClient().dispatch();

  return (uint32_t)(err0 + err1 + err2 + err3);
}

uint32_t MuQuadNode::bc0Errors(unsigned int i) const {
  std::stringstream name;
  name << "bc0_errors_" << i;
  uhal::ValWord<uint32_t> err = getNode(name.str()).read();

  getClient().dispatch();

  return (uint32_t)err;
}

uint32_t MuQuadNode::bxCntrErrors() const {
  uhal::ValWord<uint32_t> err0 = getNode("bnch_cnt_errors_0").read();
  uhal::ValWord<uint32_t> err1 = getNode("bnch_cnt_errors_1").read();
  uhal::ValWord<uint32_t> err2 = getNode("bnch_cnt_errors_2").read();
  uhal::ValWord<uint32_t> err3 = getNode("bnch_cnt_errors_3").read();

  getClient().dispatch();

  return (uint32_t)(err0 + err1 + err2 + err3);
}

uint32_t MuQuadNode::bxCntrErrors(unsigned int i) const {
  std::stringstream name;
  name << "bnch_cnt_errors_" << i;
  uhal::ValWord<uint32_t> err = getNode(name.str()).read();

  getClient().dispatch();

  return (uint32_t)err;
}

uint32_t MuQuadNode::muonCntr(unsigned int i) const {
  std::stringstream cntrName;
  cntrName << "muon_counter_" << i;
  uhal::ValWord<uint32_t> muonCntr = getNode(cntrName.str()).read();

  getClient().dispatch();

  return (uint32_t)(muonCntr);
}

uint32_t MuQuadNode::muonCntrQuad() const {
  uhal::ValWord<uint32_t> muonCntr0 = getNode("muon_counter_0").read();
  uhal::ValWord<uint32_t> muonCntr1 = getNode("muon_counter_1").read();
  uhal::ValWord<uint32_t> muonCntr2 = getNode("muon_counter_2").read();
  uhal::ValWord<uint32_t> muonCntr3 = getNode("muon_counter_3").read();

  getClient().dispatch();

  return (uint32_t)(muonCntr0 + muonCntr1 + muonCntr2 + muonCntr3);
}

uint32_t MuQuadNode::oneNominalShowerCntr(unsigned int i) const {
  std::stringstream cntrName;
  cntrName << "one_nominal_shower_counter_" << i;
  uhal::ValWord<uint32_t> muonShowerCntr = getNode(cntrName.str()).read();

  getClient().dispatch();

  return (uint32_t)(muonShowerCntr);
}

uint32_t MuQuadNode::oneNominalShowerCntrQuad() const {
  uhal::ValWord<uint32_t> muonShowerCntr0 =
      getNode("one_nominal_shower_counter_0").read();
  uhal::ValWord<uint32_t> muonShowerCntr1 =
      getNode("one_nominal_shower_counter_1").read();
  uhal::ValWord<uint32_t> muonShowerCntr2 =
      getNode("one_nominal_shower_counter_2").read();
  uhal::ValWord<uint32_t> muonShowerCntr3 =
      getNode("one_nominal_shower_counter_3").read();

  getClient().dispatch();
  return (uint32_t)(muonShowerCntr0 + muonShowerCntr1 + muonShowerCntr2 +
                    muonShowerCntr3);
}

uint32_t MuQuadNode::oneTightShowerCntr(unsigned int i) const {
  std::stringstream cntrName;
  cntrName << "one_tight_shower_counter_" << i;
  uhal::ValWord<uint32_t> muonShowerCntr = getNode(cntrName.str()).read();

  getClient().dispatch();

  return (uint32_t)(muonShowerCntr);
}

uint32_t MuQuadNode::oneTightShowerCntrQuad() const {
  uhal::ValWord<uint32_t> muonShowerCntr0 =
      getNode("one_tight_shower_counter_0").read();
  uhal::ValWord<uint32_t> muonShowerCntr1 =
      getNode("one_tight_shower_counter_1").read();
  uhal::ValWord<uint32_t> muonShowerCntr2 =
      getNode("one_tight_shower_counter_2").read();
  uhal::ValWord<uint32_t> muonShowerCntr3 =
      getNode("one_tight_shower_counter_3").read();

  getClient().dispatch();
  return (uint32_t)(muonShowerCntr0 + muonShowerCntr1 + muonShowerCntr2 +
                    muonShowerCntr3);
}

uint32_t MuQuadNode::oneLooseShowerCntr(unsigned int i) const {
  std::stringstream cntrName;
  cntrName << "one_loose_shower_counter_" << i;
  uhal::ValWord<uint32_t> muonShowerCntr = getNode(cntrName.str()).read();

  getClient().dispatch();

  return (uint32_t)(muonShowerCntr);
}

uint32_t MuQuadNode::oneLooseShowerCntrQuad() const {
  uhal::ValWord<uint32_t> muonShowerCntr0 =
      getNode("one_loose_shower_counter_0").read();
  uhal::ValWord<uint32_t> muonShowerCntr1 =
      getNode("one_loose_shower_counter_1").read();
  uhal::ValWord<uint32_t> muonShowerCntr2 =
      getNode("one_loose_shower_counter_2").read();
  uhal::ValWord<uint32_t> muonShowerCntr3 =
      getNode("one_loose_shower_counter_3").read();

  getClient().dispatch();
  return (uint32_t)(muonShowerCntr0 + muonShowerCntr1 + muonShowerCntr2 +
                    muonShowerCntr3);
}

} // namespace ugmt
