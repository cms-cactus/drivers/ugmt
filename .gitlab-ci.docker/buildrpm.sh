#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t'

cd `dirname $0`/..
imagename=gitlab-registry.cern.ch/cms-cactus/core/swatch/swatch-buildenv-665b4159a49d
dockerdir=/xdaq/makefiles/demand/a/ridiculously/long/path/wherin/to/put/your/project/fun/fun/fun

docker run --mount type=bind,source="$(pwd)",target=$dockerdir $imagename "cd $dockerdir && rm -rf */rpm && make clean && make rpm"

